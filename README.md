# Site web de test de MISIS

- *Documentation au format DSFR* (Lien en construction)
- [Wiki pour modifier les pages](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/misis/misis-test-website/-/wikis/)
- [Pour déployer le contenu du wiki](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/misis/misis-test-website/-/pipelines/new) -> puis cliquer sur le bouton "Run pipeline"

> ⚠️ **Travail en cours** ⚠️

## Début de documentation

- Le titre du site et le sous-titre reprennent directement le titre du projet Gitlab et sa description
- Toujours utiliser du markdown dans le wiki
- Possibilité d'inclure des images externes
- On peut utiliser l'insertion des diagrammes du wiki (qui fabrique un svg qui est ensuite importé comme image)
- Le menu latéral du wiki Gitlab devient le menu principale de navigation en DSFR. Seuls les liens avec titre vers les pages internes au Wiki sont pris en compte.
  - Il est possible d'avoir un menu sur deux niveaux à condition que :
    - La barre latérale se présente comme une liste à puces
    - Les sous-menus ont une indentation (série d'espaces) en début de ligne
    - Le menu englobant les sous-menus est juste un texte et non un lien
    - Exemple :

    ```markdown
    - [Accueil](Home)
    - Documents
        - [Mode d'emploi](Mode-d'emploi)
        - [Local Document](Local-Doc)
    - [Autre lien](Sample-Markdown)
    ```

## Déploiement

`Build -> Pipelines -> Run pipeline en haut à droite`
